const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js")
const auth = require("../auth.js")


router.get("/all", (req, res) =>{

	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});

router.get("/active", (req, res) => {

	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))

});

router.post("/create", auth.verify, (req, res) => {


	const data = {
		product: req.body, 
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	if(data.isAdmin) {

	productController.addProduct(data.product).then(resultFromController => res.send(resultFromController))

	} else {

		res.send(false);
	}

	

});


// for retrieving a specific product
router.get("/:productId", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
});

// Updating a product 
router.put("/update/:productId", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {
		productController.updateProduct(data.product, data.params).then(resultFromController => res.send(resultFromController))
	} else {

		res.send(false);
	}
});

// Archiving a Product 
router.patch("/:productId/archive", auth.verify, (req, res) => {

	const data = {
		product: req.body, 
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {
		productController.archiveProduct(data.product, data.params).then(resultFromController => res.send(resultFromController))
	} else {

		res.send(false);
	}
});


module.exports = router;
