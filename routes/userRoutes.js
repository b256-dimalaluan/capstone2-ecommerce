const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");
const auth = require("../auth.js")

//register
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// user authentication / login 
router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Router for Non-admin User checkout/create order
router.post("/checkout", (req, res) => {

	const data = {
		userId: req.body.userId,
		productId: req.body.productId,
		quantity: req.body.quantity,
		totalAmount: req.body.totalAmount,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {

		res.send(false)

	} else {

		userController.checkoutUser(data).then(resultFromController => res.send(resultFromController))
	}
	
});

router.get("/:userId/userDetails", auth.verify, (req, res) => {

	const data = {

		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin) {

		userController.getUserDetail(req.params).then(resultFromController => res.send(resultFromController))
	} else {

		res.send(false)
	}

});


// set user as an admin(admin auth)
// Archiving a Product 
router.patch("/:userId/setadmin", auth.verify, (req, res) => {

	const data = {
		user: req.body, 
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin) {
		userController.setAdmin(data.user, data.params).then(resultFromController => res.send(resultFromController))
	} else {

		res.send(false);
	}
});

module.exports = router;