
const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");

const app = express();


app.use(express.json());
app.use(express.urlencoded({extended: true}));

 
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.nrhduvh.mongodb.net/B256_E-CommerceAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


app.use("/users", userRoutes)
app.use("/products", productRoutes)

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`Sa wakas! Connected to the cloud database na po`));


//Server Listening }
app.listen(4000, () => console.log(`Hello! API is now online po on port 4000`));
