const User = require("../models/User.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js")


module.exports.registerUser = (requestBody) => {

	let newUser = new User({
		email: requestBody.email,
		password: bcrypt.hashSync(requestBody.password, 10)
	})

	return newUser.save().then((user, err) => {

		if(err) {

			return false;
		} else {

			return true;
		}
	})

};

module.exports.authenticateUser = (requestBody) => {

	return User.findOne({email: requestBody.email}).then(result => {

		if(result == null) {

			return false;

		}
		else {

			

			const isPasswordCorrect = bcrypt.compareSync(requestBody.password, result.password)

			
			if(isPasswordCorrect) {
				
				return {access: auth.createAccessToken(result)}

			} 
			else {

				return false;
			}
		}
	})
};

module.exports.checkoutUser = async (data) => {


	let productPrice = Product.findById(data.productId).then(product => {

		console.log(product.price);

		return product.price;
	});

	let orderTotalAmount = productPrice * data.quantity;

	console.log(orderTotalAmount);

	// let totalAmount = orderTotalAmount;


	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.orderedProduct.push({

			products: [{
				productId: data.productId,
				quantity: data.quantity


			}]

		})

		user.orderedProduct.push({totalAmount: data.totalAmount})

	

		return user.save().then((ordered, err) => {

			if(err) {

				return false;

			} else {

				return true;
			}
		})
	})

	let isProductUpdated = await Product.findById(data.productId).then(product => {

		product.userOrders.push({userId: data.userId});

		return product.save().then((enrolled, err) => {

			if(err) {

				return false;

			} else {

				return true;
			}
		})
	})

	// totalAmount: {Product.price * User.orderedProducts.quantity}



	if(isUserUpdated && isProductUpdated) {

		return true;
	} else {

		return false;
	}
};

module.exports.getUserDetail = (reqParams) => {

	return User.findById(reqParams.userId).then(result => {

		return result;
	})
};

module.exports.setAdmin = (user, paramsId) => {

	let setUserAdmin = {
		isActive: true
	}

	return User.findByIdAndUpdate(paramsId.userId, setUserAdmin).then((result, err) => {

		if (err) {
			return false;
		} else {
			return true;
		}
	})

};