const Product = require("../models/Product.js");
const auth = require("../auth.js")


module.exports.getAllProducts = () => {

	return Product.find().then( result => {

		return result;
	})
};

module.exports.getActiveProducts = () => {

	return Product.find({isActive: true}).then( result => {

		return result;
	})
};

module.exports.addProduct = (product) => {

	let newProduct = new Product({
		name: product.name,
		description: product.description,
		price: product.price
	});

	
	return newProduct.save().then((result, err) => {

		if(err) {
			return false;
		
		} else {
			return true;
		}
	})

};

module.exports.getProduct = (reqParams) => {

	return Product.findById(reqParams.productId).then( result =>{

		return result;
	})
};


module.exports.updateProduct = (product, paramsId) => {

	let updatedProduct = {
		name:product.name,
		description: product.description,
		price: product.price
	}

	return Product.findByIdAndUpdate(paramsId.productId, updatedProduct).then((result, err) => {

		if(err) {
			return false;
		} else {
			return true;
		}
	})
};


module.exports.archiveProduct = (product, paramsId) => {

	let archivedProduct = {
		isActive: false
	}
	return Product.findByIdAndUpdate(paramsId.productId, archivedProduct).then((result, err) => {

		if (err) {
			return false;
		} else {
			return true;
		}
	})

};