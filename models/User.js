const mongoose = require("mongoose");
const Product = require("../models/Product.js");
const userSchema = new mongoose.Schema({

	email: {
		type: String,
		required: [true, "Email Address is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orderedProduct: [{

		products: [{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			quantity:{
				type: Number, 
				required: true
			}
		}],

		totalAmount:{
			type: Number,
		},
		purchasedOn:{
			type: Date,
			default: new Date()
		}
		

	}],
	

});

module.exports = mongoose.model("User", userSchema);
